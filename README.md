## Enabling Serverless Security Analytics using AWS WAF logs, Amazon Athena, and Amazon QuickSight

This tutorial goes through the steps on how we can analyze AWS WAF (Web Application Firewall) logs through setting up a Kinesis Data Firehose stream, an AWS Glue Crawler, an AWS Athena table and visualize these logs in AWS QuickSight.  

As AWS WAF logs frequently monitors requests from AWS Resources, there is a large amount of data to be collected and analyzed.  AWS services allow for large amounts of data to be collected and analyzed, without using any servers.  

AWS Kinesis Data Firehose can be used to load streaming data (AWS WAF Logs), which is then delivered to S3.  AWS Glue Crawler then transfer the data into a table, which can be queried and viewed in AWS Athena.  AWS QuickSight can then be used to visualize the data, allowing the user to analyze the data.   

## Outline of the Tutorial
1.	The AWS WAF Logs are delivered to an AWS S3 bucket via AWS Kinesis Data Firehose 

2.	As the AWS WAF logs contain a lot of information, AWS Glue Crawler can be used to transfer the data (AWS WAF Logs) into AWS Glue in the form of a table 

3.	The table created by the AWS Glue Crawler can be viewed in AWS Athena 
Images/.	AWS QuickSight can be used to visualize the AWS WAF logs 

## AWS Architecture 
<p align="center">
    <img src="Images/Architecture.png" width="90%" height="90%">
</p>

## Components of the Tutorial 
### AWS WAF 
- Monitors HTTP and HTTPS request that are forwarded to an Amazon API Gateway, CloudFront or Application Load Balancer.  
  
- Controls access to your content: API Gateway, CloudFront or Application Load Balancer responds to request with the requested content or HTTP 403 status code (Forbidden) 

### AWS Kinesis Data Firehose 
- Real-time streaming data 
  
- Collect the AWS WAF Logs and deliver the logs to S3 

### AWS Glue Crawler 
- Transfer the AWS WAF logs into AWS Glue in the form of a table 

### AWS Athena 
- Interactive query service 
  
- Analyze data using standard SQL 

### AWS QuickSight 
- Business analytics service used for visualizations 

**SPICE**  
- SPICE stands for Super-fast, Parallel, In-memory Calculation Engine
  
- SPICE is Amazon QuickSight's in-memory optimized calculation engine
  
- Designed specifically for fast, ad hoc data visualization

## Tutorial 
### Prerequisites 
- Region: N.Virginia 
  
- Download the CloudFormation Temple: [ALB_WAF_Tutorial.yaml](Files/ALB_WAF_Tutorial.yaml) 

---

### Create S3 Bucket 
- Navigate to S3 in the console 

- Click **"Create Bucket"**
<p align="center">
    <img src="Images/1.jpg" width="80%" height="80%">
</p>

- Input a name for the bucket 

- Click **"Create"** 

<p align="center">
    <img src="Images/2.jpg" width="80%" height="80%">
</p>

--- 

### Create a CloudFormation Stack 
The CloudFormation stack will create an AWS Infrastructure that contains an Application Load Balancer.  AWS WAF will monitor the HTTP and HTTPS request from this Application Load Balancer. This Application Load Balancer will be associated with the Web ACL created in this tutorial.   

The AWS Architecture and CloudFormation Stack for the Application Load Balancer can be found [here](https://github.com/aws-samples/ecs-refarch-cloudformation).  

However, if you already have an AWS resource that you want to associate with the Web ACL such as CloudFront, API Gateway or an existing Application Load Balancer, you can skip this step and continue to "Create a Kinesis Firehose".  

- Navigate to CloudFormation in the console 

- Under "Step 1: Specify Template", choose **“Template is ready”** and **“Upload a template file”**

<p align="center">
    <img src="Images/3.jpg" width="80%" height="80%">
</p>

- Click on **Choose file”**

- Upload the file “ALB_WAF_Tutorial.yaml”  

> This is the template for the Application Load Balancer 

- Click **"Next”** 

<p align="center">
    <img src="Images/4.jpg" width="80%" height="80%">
</p>

- Under "Step 2: Specify stack details", input a name for the CloudFormation Stack 

<p align="center">
    <img src="Images/5.jpg" width="80%" height="80%">
</p>

- Click **"Next"** until you reach the "Review" tab 

- In "Step Images/: Review", scroll down and tick both boxes 

- Click **Create Stack"**

<p align="center">
    <img src="Images/6.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/7.png" width="80%" height="80%">
</p>

- Wait until the CloudFormation Stack status changes to "Create Complete" 
<p align="center">
    <img src="Images/8.jpg" width="80%" height="80%">
</p>

--- 

### Create a Kinesis Firehose 
AWS Kinesis Firehose will transfer the logs from the AWS WAF to the S3 Bucket 

- Navigate to Kinesis in the console

- Select **"Data Firehouse"** from the dashboard 

- Click **"Create delivery stream"**

<p align="center">
    <img src="Images/9.jpg" width="80%" height="80%">
</p>

- For **"Delivery stream name"**, input a name with the format: `"aws-waf-logs-yourname"` 

> Make sure the name of the delivery stream contains **"aws-waf-logs-"** as this is needed for AWS WAF to detect the delivery stream 

<p align="center">
    <img src="Images/10.jpg" width="80%" height="80%">
</p>
  
- For **"Source"**, select **"Direct Put"** 
> This ensures that the source of the delivery stream is AWS WAF

<p align="center">
    <img src="Images/11.jpg" width="80%" height="80%">
</p>

- Click **"Next"** 

- In "Step 2: Process records", keep both **"Record transformations"** and **"Record format conversion"** as disabled 

> Record Transformation are used for AWS Lambda functions and Record format conversion are used to convert JSON to other formats such as parquet.  However, this lab does not require these functions.   

- Click **"Next"** 
<p align="center">
    <img src="Images/12.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/13.jpg" width="80%" height="80%">
</p>

- In "Step 3: Choose destination", select **"Amazon S3"** 
> This will save the data from the delivery stream in the S3 bucket created 

<p align="center">
    <img src="Images/14.jpg" width="80%" height="80%">
</p>

- Choose the S3 bucket created at the start of the tutorial 

- Click **"Next"**

<p align="center">
    <img src="Images/15.jpg" width="80%" height="80%">
</p>

- In "Step Images/: Configure settings", navigate to **"IAM Role"** 

- Click **"Create new or choose"** 
<p align="center">
    <img src="Images/16.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/17.jpg" width="80%" height="80%">
</p>

- A new page will open and the IAM Role should display "kinesis_delivery_role"

- Leave all the settings as default and click **"Allow:** 

<p align="center">
    <img src="Images/18.jpg" width="80%" height="80%">
</p>

- The **"IAM Role"** should now display "kinesis_delivery_role"

- Click **"Next"** 
<p align="center">
    <img src="Images/19.png" width="80%" height="80%">
</p>

- In Step 5: Review, review all settings 

- Click **"Create Delivery Stream"** 
<p align="center">
    <img src="Images/20.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/21.png" width="80%" height="80%">
</p>

- Wait until the Delivery Stream Status becomes **"Active"** 

<p align="center">
    <img src="Images/22.jpg" width="80%" height="80%">
</p>

--- 
### AWS WAF 
AWS WAF monitors the HTTP and HTPPS Request from the Application Load Balancer and collects the data. 

- Navigate to WAF from the console 

- Click **"Go to AWS WAF"** 
<p align="center">
    <img src="Images/23.jpg" width="80%" height="80%">
</p>

- Navigate to **"Web ACLs"** in the Dashboard 

- Click **"Create Web ACLs"** 
<p align="center">
    <img src="Images/24.jpg" width="80%" height="80%">
</p>

- Input a name for the Web ACL 

- For **"Region"**, select US East (N.Virginia)

- For **"Resource Type to associate with web ACL"**, select **"Application Load Balancer"** 

> This tutorial will associate the Web ACL with the Application Load Balancer created by the CloudFormation template.  

> If you already have an AWS Resource that you want to associate with the Web ACL, choose your own AWS Resource for this next step.   

- For **"AWS resource to associate"**, select the Application Load Balancer created by the CloudFormation Stack 
  
> The name of the Application Load Balancer is the same as the name of the CloudFormation stack 

- Click **"Next"** 

<p align="center">
    <img src="Images/25.jpg" width="80%" height="80%">
</p>

> If you want to associate CloudFront to the Web ACL, select "Global" for Region 

> If you want to select your own API Gateway or Application Load Balancer, select the region where the AWS resource is located 

- In Step 2: Create Conditions, we can create the conditions for the Web ACL 

> For this tutorial, we have chosen to associate the Web ACL with the Application Load Balancer and we will create an IP Match condition. 

> If you have selected your AWS Resource to associate with the Web ACL, you can choose your own condition in this step 

- Select **"IP match conditions"** and click **"Create Condition"** 

<p align="center">
    <img src="Images/26.jpg" width="80%" height="80%">
</p>


<p align="center">
    <img src="Images/27.jpg" width="80%" height="80%">
</p>

- Input a name for the IP match condition 
  
- Select **"IPv4"** for IP Version 
  
- Click **"Create"** 

<p align="center">
    <img src="Images/28.png" width="80%" height="80%">
</p>

- Ensure that the Condition creation is successful 

<p align="center">
    <img src="Images/29.jpg" width="80%" height="80%">
</p>

- Click **"Next"** 

- In Step 3: Create Rules, for **"Default Action"**, select **"Allow all requests that don't match any rules"** 

- Click **"Review and create"** 
  
<p align="center">
    <img src="Images/30.jpg" width="80%" height="80%">
</p>

- In Step Images/: Review and Create, review all the settings 
  
- Click **"Confirm and create"** 
  
<p align="center">
    <img src="Images/31.png" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/32.png" width="80%" height="80%">
</p>

--- 

### Enable Logging for AWS WAF 
Enable logging for WAF so that Kinesis Firehose can collect data from AWS WAF 

- Select the WAF Created 

- Navigate to the **"Logging"** tab 

- Click **"Enable Logging"** 

<p align="center">
    <img src="Images/33.jpg" width="80%" height="80%">
</p>

- For **"Amazon Kinesis Data Firehose"**, select the Kinesis Firehose created 

- Click **"Create"** 

<p align="center">
    <img src="Images/34.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/35.png" width="80%" height="80%">
</p>

*For reference* 
<p align="center">
    <img src="Images/10.jpg" width="50%" height="50%">
</p>

- Check that "Logging" has successfully enabled 
<p align="center">
    <img src="Images/36.png" width="80%" height="80%">
</p>

### Once Logging is enabled, the S3 bucket created can recieve logs/data from the WAF via Kinesis Firehose

> The S3 bucket created at the beginning of the tutorial should contain data from the AWS WAF.  The files are stored in order of "year, month, day and hour".  

<p align="center">
    <img src="Images/37.png" width="80%" height="80%">
</p>

Check that the Web ACL is recording the Web Request from the Application Load Balancer 

- Navigate to the **"Requests"** tab 

- Click on the link **"View the Graph in CloudWatch"** 

<p align="center">
    <img src="Images/38.jpg" width="80%" height="80%">
</p>

- The CloudWatch graph displays the data from the Web ACL created 
<p align="center">
    <img src="Images/39.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/40.png" width="80%" height="80%">
</p>

--- 

### Create AWS Glue Crawler 
Transfer the AWS WAF Logs from S3 into a table in AWS Glue 
- Navigate to AWS Glue from the console 

- In the Dashboard, navigate to **"Crawler"** 

- Click **"Add Crawler"**
<p align="center">
    <img src="Images/41.jpg" width="80%" height="80%">
</p>

- Input a name for the Crawler 

- Click **"Next"** 
<p align="center">
    <img src="Images/42.png" width="80%" height="80%">
</p>

- For **"Crawler source type"** select **"Data Store"** 

- Click **"Next"** 
<p align="center">
    <img src="Images/43.png" width="80%" height="80%">
</p>

- For **"Choose a data store"** select S3 

- For **"Include Path"**, select the S3 bucket created 

<p align="center">
    <img src="Images/44.jpg" width="80%" height="80%">
</p>

- For **"Add another data store"**, select **"No"** 

- Click **"Next"** 

<p align="center">
    <img src="Images/45.png" width="80%" height="80%">
</p>

- For **"Choose an IAM Role"**, select **"Create an IAM Role"** 

- Input a name for the IAM Role 

- Click **"Next"** 

<p align="center">
    <img src="Images/46.png" width="80%" height="80%">
</p>

- For **"Frequency"**, select **"Run on demand"** 

- Click **"Next"** 

<p align="center">
    <img src="Images/47.jpg" width="80%" height="80%">
</p>

- For **"Database"**, select **"Add database"** 

<p align="center">
    <img src="Images/48.jpg" width="80%" height="80%">
</p>

- For **"Database name"**, input a name with the format: ``"yournamedb"``
  
> Make sure that the database name does not contain any spaces 

<p align="center">
    <img src="Images/49.jpg" width="80%" height="80%">
</p>

- Click **"Next"** 

<p align="center">
    <img src="Images/50.jpg" width="80%" height="80%">
</p>

- Review all the settings and click **"Finish"** 

<p align="center">
    <img src="Images/51.jpg" width="80%" height="80%">
</p>

- After the crawler is created, a prompt will appear 
  
- Click **"Run it now"**
<p align="center">
    <img src="Images/52.jpg" width="80%" height="80%">
</p>

- The crawler will start to run 
<p align="center">
    <img src="Images/53.jpg" width="80%" height="80%">
</p>

- When the crawler has finished running, the prompt will show **"Crawler is now running"**
<p align="center">
    <img src="Images/54.jpg" width="80%" height="80%">
</p>

- After the crawler has run successfully, the crawler will create a table 

- The **"Tables added"** column should show 1 table created

<p align="center">
    <img src="Images/55.jpg" width="80%" height="80%">
</p>
                      
> If the crawler did not run successfully, click **"Run crawler"** to try again 

--- 

### View table created by Crawler in Athena 

- Navigate to Athena in the console 

- For **"Database"**, select the database created from the crawler 

*For Reference* 
<p align="center">
    <img src="Images/49.jpg" width="50%" height="50%">
</p>

- The database should display the table created by the crawler 

- Select the table and click **"Preview Table"** 

<p align="center">
    <img src="Images/56.jpg" width="80%" height="80%">
</p>

- Alternatively, you can also enter the following information in the "New Query" Box to preview the table 

```
SELECT * FROM "waftutorialdb"."waf_tutorial" limit 10;
```
> Replace "waftutorialdb" with the name of your database and replace "waf_tutorial" with the name of your table 

- Click **"Run Query"** 

<p align="center">
    <img src="Images/57.jpg" width="80%" height="80%">
</p>

- The data from the table will be shown below 
<p align="center">
    <img src="Images/58.png" width="80%" height="80%">
</p>

--- 

### Visualize the data in QuickSight 

- Navigate to QuickSight from the console 

- If it is your first time logging into QuickSight, you may have to enter your email to create a QuickSight account

- On the QuickSight homepage, navigate to your account in the top right 

- Click on **"Manage QuickSight"** 

<p align="center">
    <img src="Images/59.jpg" width="80%" height="80%">
</p>

- Navigate to **"Security and permissions"** and under **"QuickSight access to AWS Services"**, click **"Add or Remove"** 

<p align="center">
    <img src="Images/60.jpg" width="80%" height="80%">
</p>

- Navigate to **"Amazon S3"** 

- Click **"Details"** 

<p align="center">
    <img src="Images/61.jpg" width="80%" height="80%">
</p>

- Click **"Select S3 Buckets"** 
<p align="center">
    <img src="Images/62.jpg" width="80%" height="80%">
</p>

- Select the S3 Bucket created at the start of the tutorial 
<p align="center">
    <img src="Images/63.jpg" width="80%" height="80%">
</p>

- Under **"Amazon S3"**, there should be 1 bucket selected 

- Click **"Update"** 
<p align="center">
    <img src="Images/64.jpg" width="80%" height="80%">
</p>

- Navigate to the QuickSight Homepage 

- In the top left, click **"New analysis"** 

<p align="center">
    <img src="Images/65.jpg" width="80%" height="80%">
</p>

- Click **"New data set"** 

<p align="center">
    <img src="Images/66.jpg" width="80%" height="80%">
</p>

- Select **"Athena"** for data source 

<p align="center">
    <img src="Images/67.jpg" width="80%" height="80%">
</p>

- Input a name for your QuickSight Analysis 

- Click **"Create data source"** 

<p align="center">
    <img src="Images/68.jpg" width="80%" height="80%">
</p>

- For **"Choose your table"**, select **"Use custom SQL"**
<p align="center">
    <img src="Images/69.jpg" width="80%" height="80%">
</p>

- Enter the following SQL Code in the box below 

```
 with d as (select
    waf.timestamp,
        waf.formatversion,
        waf.webaclid,
        waf.terminatingruleid,
        waf.terminatingruletype,
        waf.action,
        waf.httpsourcename,
        waf.httpsourceid,
        waf.HTTPREQUEST.clientip as clientip,
        waf.HTTPREQUEST.country as country,
        waf.HTTPREQUEST.httpMethod as httpMethod,
        map_agg(f.name,f.value) as kv
    from waftutorialdb.waf_tutorial waf,
    UNNEST(waf.httprequest.headers) as t(f)
    group by 1,2,3,4,5,6,7,8,9,10,11)
    select d.timestamp,
        d.formatversion,
        d.webaclid,
        d.terminatingruleid,
        d.terminatingruletype,
        d.action,
        d.httpsourcename,
        d.httpsourceid,
        d.clientip,
        d.country,
        d.httpMethod,
        d.kv['Host'] as host,
        d.kv['User-Agent'] as UA,
        d.kv['Accept'] as Acc,
        d.kv['Accept-Language'] as AccL,
        d.kv['Accept-Encoding'] as AccE,
        d.kv['Upgrade-Insecure-Requests'] as UIR,
        d.kv['Cookie'] as Cookie,
        d.kv['X-IMForwards'] as XIMF,
        d.kv['Referer'] as Referer
    from d;
```
> In Line 14 
> ```
> from waftutorialdb.waf_tutorial waf, 
> ```
> Replace **waftutorialdb** with the name of your database and **waf_tutorial** with the name of your table.  

> The name of the database and table is the same as the one shown in Athena 

*For reference* 
<p align="center">
    <img src="Images/57.jpg" width="60%" height="60%">
</p>

- Click **"Confirm query"** 

<p align="center">
    <img src="Images/70.jpg" width="80%" height="80%">
</p>

- Click **"Edit/Preview Data"** 

<p align="center">
    <img src="Images/71.jpg" width="80%" height="80%">
</p>

- Under **"Data source"**, select **"SPICE"** 

- Navigate to the **"Fields"** section 

<p align="center">
    <img src="Images/72.jpg" width="80%" height="80%">
</p>

- Select **"Timestamp"**

- Click **"Change data type"** and select **"Date"**

<p align="center">
    <img src="Images/73.jpg" width="80%" height="80%">
</p>

- Ensure that the data type for **"Timestamp"** is **"Date"** 

- Click **"Save"** 

<p align="center">
    <img src="Images/74.jpg" width="80%" height="80%">
</p>

- Navigate to **"Your Data Sets"** 

- Select the data set created.  This is listed as **"New Custom SQL"**

<p align="center">
    <img src="Images/75.jpg" width="80%" height="80%">
</p>

Click **"Create Analysis"** 

<p align="center">
    <img src="Images/76.jpg" width="80%" height="80%">
</p>

- You can now visualize the WAF logs in QuickSight 

--- 

### QuickSight Visualizations 

- Below are some example visualizations done on QuickSight 

- You are able to choose different fields from the **"Field List"** and different types of Visualizations from **"Visual Types"** 

<p align="center">
    <img src="Images/77.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/78.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/79.jpg" width="80%" height="80%">
</p>

## Conclusion 
This tutorial goes through the steps of collecting and visualizing AWS WAF logs.  

This tutorial can be expanded further by automating the streaming of data (AWS WAF Logs) or by adding Amazon Lambda functions to transform the data.  

## Delete AWS Resources 
- CloudFormation Stack
  
- S3 Bucket
  
- Kinesis Firehose
  
- WAF
  
- Glue Crawler
  
- Glue Database
  
- QuickSight Analysis 
